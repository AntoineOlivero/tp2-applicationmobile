package com.example.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";

    public Context context;

    public static long id = 0;

    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        // db.execSQL() with the CREATE TABLE ... command
        db.execSQL("CREATE TABLE cellar ("+_ID+" NUMERIQ, "
                +COLUMN_NAME+" TEXT, "
                +COLUMN_WINE_REGION+" TEXT,"
                +COLUMN_LOC+" TEXT,"
                +COLUMN_CLIMATE+" TEXT,"
                +COLUMN_PLANTED_AREA+" TEXT, UNIQUE ("+COLUMN_NAME+", "+COLUMN_WINE_REGION+
                ")ON CONFLICT ROLLBACK);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Inserting Row
        ContentValues contentValues = new ContentValues();

        contentValues.put(_ID, WineDbHelper.id);
        WineDbHelper.id = WineDbHelper.id + 1;
        contentValues.put(COLUMN_NAME, wine.getTitle());
        contentValues.put(COLUMN_WINE_REGION, wine.getRegion());
        contentValues.put(COLUMN_LOC, wine.getLocalization());
        contentValues.put(COLUMN_CLIMATE, wine.getClimate());
        contentValues.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());

        long insertChecker = 0;
        insertChecker = db.insert(TABLE_NAME, null, contentValues);

        db.close(); // Closing database connection

        if (this.context != null) {
            if (insertChecker == -1) {
                Toast.makeText(this.context, "Votre vin n'a pas pus être ajouté au cellier", Toast.LENGTH_LONG).show();
            }
            else {
                Toast.makeText(this.context, "Votre Vin a été ajouté au Cellier", Toast.LENGTH_LONG).show();
            }
        }
        return (insertChecker != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
        int updateChecker = 0;

        // updating row
        // call db.update()
        ContentValues contentValues = new ContentValues();
        contentValues.put(_ID, wine.getId());
        contentValues.put(COLUMN_NAME, wine.getTitle());
        contentValues.put(COLUMN_WINE_REGION, wine.getRegion());
        contentValues.put(COLUMN_LOC, wine.getLocalization());
        contentValues.put(COLUMN_CLIMATE, wine.getClimate());
        contentValues.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());

        updateChecker = db.update(TABLE_NAME, contentValues, "_id=" + wine.getId(), null);
        db.close();

        return updateChecker;
    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM cellar;", null);
        // call db.query()

        return cursor;
    }

    public void deleteWine(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        // call db.delete();
        db.delete(TABLE_NAME, COLUMN_NAME + "=" + cursorToWine(cursor).getTitle(), null);
        db.close();
    }

    public void deleteWineByName(String name) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.delete(TABLE_NAME, COLUMN_NAME + "='" + name + "'", null);
        sqLiteDatabase.close();
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
        addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));



        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }


    public static Wine cursorToWine(Cursor cursor) {

        // build a Wine object from cursor
        long wineId = cursor.getLong(cursor.getColumnIndex(_ID));
        String wineName = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
        String wineRegion = cursor.getString(cursor.getColumnIndex(COLUMN_WINE_REGION));
        String wineLocalization = cursor.getString(cursor.getColumnIndex(COLUMN_LOC));
        String wineClimate = cursor.getString(cursor.getColumnIndex(COLUMN_CLIMATE));
        String winePlanted = cursor.getString(cursor.getColumnIndex(COLUMN_PLANTED_AREA));

        Wine wine = new Wine(wineId, wineName, wineRegion, wineLocalization, wineClimate, winePlanted);
        return wine;
    }
}
