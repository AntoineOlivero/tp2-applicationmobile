package com.example.tp2;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class RowAdapter extends ArrayAdapter<Wine> {


    public RowAdapter(Context context, Wine[] wine) {
        super(context, 0, wine);

    }

    public View getView(int position, View view, ViewGroup parent) {
        Wine wine = getItem(position);

        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.listview_row, parent, false);
        }

        TextView nameTextViewField = view.findViewById(R.id.wineName);
        TextView infoTextViewField = view.findViewById(R.id.wineInfo);

        nameTextViewField.setText(wine.getTitle());
        infoTextViewField.setText(wine.getRegion());

        return view;
    }

}
