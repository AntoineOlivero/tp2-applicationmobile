package com.example.tp2;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;


public class MainActivity extends AppCompatActivity {

    public static  WineDbHelper wineDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //création de la toolbar et du boutton settings à l'interieur
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //création du boutton +
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Ajout d'un nouveau vin dans le cellier", Snackbar.LENGTH_LONG).setAction("ajoutVin", null).show();
                Wine wine = new Wine("Nouveau vin", "", "", "", "");
                Intent wineActivity = new Intent(MainActivity.this, WineActivity.class);
                wineActivity.putExtra("newWine", wine);
                startActivity(wineActivity);

            }
        });


        MainActivity.wineDbHelper = new WineDbHelper(this);
        wineDbHelper.populate();
        Cursor cursor = wineDbHelper.fetchAllWines();
        final SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(this, R.layout.listview_row, cursor, new String[] {
                WineDbHelper.COLUMN_NAME,
                WineDbHelper.COLUMN_WINE_REGION,
                WineDbHelper.COLUMN_LOC,
                WineDbHelper.COLUMN_CLIMATE,
                WineDbHelper.COLUMN_PLANTED_AREA,
                WineDbHelper._ID
        }, new int[] {
                R.id.wineName,
                R.id.wineInfo
        },0);

        ListView listView = findViewById(R.id.listView);
        listView.setAdapter(cursorAdapter);
        //registerForContextMenu(listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListView listView1 = (ListView) findViewById(R.id.listView);
                Cursor cursor1 = (Cursor) listView1.getItemAtPosition(position);
                Wine wineSelected = WineDbHelper.cursorToWine(cursor1);
                wineSelected.setId(position);
                Intent wineActivity = new Intent(MainActivity.this, WineActivity.class);
                wineActivity.putExtra("newWine", wineSelected);
                startActivity(wineActivity);
            }
        });

    }




    //action du menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, contextMenu);
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        ListView listView = findViewById(R.id.listView);
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        int position = adapterContextMenuInfo.position;
        Cursor cursor = (Cursor) listView.getItemAtPosition(position);
        Wine wineSelected = WineDbHelper.cursorToWine(cursor);
        Toast.makeText(this, wineSelected.getTitle() + "sera supprimé", Toast.LENGTH_LONG).show();
        MainActivity.wineDbHelper.deleteWine(cursor);

        Intent intent = new Intent(MainActivity.this, MainActivity.class);
        startActivity(intent);
        return true;
    }
}
