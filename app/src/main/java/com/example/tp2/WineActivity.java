package com.example.tp2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class WineActivity extends AppCompatActivity {

    public static long ID;

    protected void onCreate(Bundle instance) {

        super.onCreate(instance);
        setContentView(R.layout.activity_wine);

        Wine wine = (Wine) getIntent().getExtras().get("newWine");
        WineActivity.ID = wine.getId();

        EditText wineName = (EditText) findViewById(R.id.wineName);
        wineName.setText(wine.getTitle(), TextView.BufferType.EDITABLE);

        EditText wineRegion = (EditText) findViewById(R.id.editWineRegion);
        wineRegion.setText(wine.getRegion());

        EditText wineLocalization = (EditText) findViewById(R.id.editLoc);
        wineLocalization.setText(wine.getLocalization());

        EditText wineClimate = (EditText) findViewById(R.id.editClimate);
        wineClimate.setText(wine.getClimate());

        EditText winePublisher = (EditText) findViewById((R.id.editPlantedArea));
        winePublisher.setText(wine.getPlantedArea());

        Button buttonSave = (Button) findViewById(R.id.button);

        buttonSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                EditText nameText = (EditText) findViewById(R.id.wineName);
                String newWineName = nameText.getText().toString();

                EditText regionText = (EditText) findViewById(R.id.editWineRegion);
                String newWineRegion = regionText.getText().toString();

                EditText localizationText = (EditText) findViewById(R.id.editLoc);
                String newWineLocalization = localizationText.getText().toString();

                EditText climateText = (EditText) findViewById(R.id.editClimate);
                String newWineClimate = climateText.getText().toString();

                EditText publisherText = (EditText) findViewById(R.id.editPlantedArea);
                String newWinePublisher = publisherText.getText().toString();

                Wine newWine = new Wine(WineActivity.ID, newWineName, newWineRegion, newWineLocalization, newWineClimate, newWinePublisher);
                MainActivity.wineDbHelper.updateWine(newWine);

                Intent mainActivity = new Intent(WineActivity.this, MainActivity.class);
                mainActivity.putExtra("wine", newWine);
                startActivity(mainActivity);
            }
        });
    }
}
